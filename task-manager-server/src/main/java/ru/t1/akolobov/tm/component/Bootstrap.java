package ru.t1.akolobov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.*;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.*;
import ru.t1.akolobov.tm.endpoint.*;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.SessionRepository;
import ru.t1.akolobov.tm.repository.TaskRepository;
import ru.t1.akolobov.tm.repository.UserRepository;
import ru.t1.akolobov.tm.service.*;
import ru.t1.akolobov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            userRepository,
            projectRepository,
            taskRepository,
            propertyService
    );
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(
            projectService,
            taskService,
            userService
    );
    private final Backup backup = new Backup(domainService);
    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        projectService.create("1", "TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("1", "DEMO PROJECT", Status.COMPLETED);
        projectService.create("2", "BETA PROJECT", Status.NOT_STARTED);
        projectService.create("2", "BEST PROJECT", Status.IN_PROGRESS);
        taskService.create("1", "TEST TASK", Status.IN_PROGRESS);
        taskService.create("1", "DEMO TASK", Status.COMPLETED);
        taskService.create("2", "BETA TASK", Status.NOT_STARTED);
        taskService.create("2", "BEST TASK", Status.IN_PROGRESS);
        userService.create("user1", "user1", "user@mail.ru").setId("2");
        userService.create("akolobov", "akolobov", Role.ADMIN).setId("1");
    }

    public void run() {
        initPid();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

}
