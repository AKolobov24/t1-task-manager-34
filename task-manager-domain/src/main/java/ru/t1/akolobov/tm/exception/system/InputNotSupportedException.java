package ru.t1.akolobov.tm.exception.system;

public final class InputNotSupportedException extends AbstractSystemException {

    public InputNotSupportedException() {
        super("Error! Input data is not supported...");
    }

}
