package ru.t1.akolobov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.ProjectStartByIndexRequest;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start project.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        getProjectEndpoint().startByIndex(request);
    }

}
