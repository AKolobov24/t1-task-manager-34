package ru.t1.akolobov.tm.command.system;


import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        ApplicationAboutResponse response = getSystemEndpoint().getAbout(new ApplicationAboutRequest());
        System.out.printf("Developer: %s\n", response.getName());
        System.out.printf("e-mail: %s\n", response.getEmail());
    }

}
