package ru.t1.akolobov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
